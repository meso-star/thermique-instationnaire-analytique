      subroutine get_temperature(debug,dim,type,
     &     boundary_index,probe_x,probe_time,T)
      implicit none
      include 'mpif.h'
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to make a single MC realization of the temperature at a
c     given position (fluid, boundary, solid) and at a given time
c     
c     Input:
c       + debug: true if debug mode
c       + dim: dimension of space
c       + type: temperature type
c         type=1: temperature at a boundary (defined by boundary_type)
c         type=2: temperature at a given position (defined by probe_x) in the solid
c       + boundary_index: index of the boundary if type=1
c         - boundary_index=1: x=x_min
c         - boundary_index=2: x=x_max
c         - boundary_index=3: y=y_min
c         - boundary_index=4: y=y_max
c         - boundary_index=5: z=z_min
c         - boundary_index=6: z=z_max
c       + probe_x: value of (x,y,z) [m] if type=2
c       + probe_time: value of time [s]
c     
c     Output:
c       + T: realization of the temperature [K]
c     
c     I/O
      logical debug
      integer dim
      integer type
      integer boundary_index
      double precision probe_x(1:Nvec_mx)
      double precision probe_time
      double precision T
c     temp
      double precision time,x(1:Nvec_mx)
      integer bindex
      double precision delta_boundary
      double precision delta_solid
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine get_temperature'

      time=probe_time
      if (type.eq.1) then       ! temperature at a boundary
         bindex=boundary_index
         delta_boundary=emin/bfactor
         call Tboundary(debug,dim,time,bindex,delta_boundary,T)
      else if (type.eq.2) then  ! temperature in the solid
         call copy_vector(dim,probe_x,x)
         delta_solid=emin/sfactor
         call Tsolid(debug,dim,time,x,delta_solid,T)
      else
         call error(label)
         write(*,*) 'type=',type
         stop
      endif
      
      return
      end
      
