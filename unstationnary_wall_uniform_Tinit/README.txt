Calcul du profil de temp�rature spatio/temporel dans un mur (g�om�trie slab), avec:

- conditions aux limites:
+ � gauche: convection + flux radiatif impos�
+ � droite : convection

- condition initiale: profil stationnaire provenant des conditions aux limites suivantes:
+ � gauche: convection
+ � droite: convection
