	double precision M_H2O
	double precision M_DA
	double precision water_volumic_mass
	double precision ice_volumic_mass
	double precision Na
	double precision Rgp
	double precision gravity

	parameter(M_H2O=18.0D+0) ! molar mass of water [g/mol]
	parameter(M_DA=28.96546D+0) ! molar mass of dry air [g/mol]
	parameter(water_volumic_mass=1000.0D+0) ! volumic mass of water [kg.m^-3] @ 15°C
	parameter(ice_volumic_mass=917.0D+0) ! volumic mass of ice [kg.m^-3]
        parameter(Na=6.02214179D+23)      ! moelcule/mol (Avogadro)
        parameter(Rgp=1.013D+5*22.40D-3/273.15D+0)	  ! perfect gas constant (Pa.m^3.mol^-1.K^-1)
	parameter(gravity=8.868240D+0) ! m.s^-2 (acceleration of gravity on Venus)
