      subroutine read_data(data_file,dim,
     &     box,hc1,hc2,hr1,hr2,epsilon1,epsilon2,
     &     rho_s,Cp_s,lambda_s,P,T0,
     &     Text1,Text2,Phi_rad,Trad_ext1,Trad_ext2,
     &     bfactor,sfactor,Nterms,mc,
     &     Nevent,Nprobe,probe_positions,Ntime,time_positions)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c       + dim: dimension of space
c
c     Output:
c       + box: dimensions of the brick
c       + hc1: convective exchange coefficient on the left side [W/(m^2.K)]
c       + hc2: convective exchange coefficient on the right side [W/(m^2.K)]
c       + hr1: radiative exchange coefficient on the left side [W/(m^2.K)]
c       + hr2: radiative exchange coefficient on the right side [W/(m^2.K)]
c       + epsilon1: emissivity of the solid on the left side
c       + epsilon2: emissivity of the solid on the right side
c       + rho_s: density of the solid [kg/m^3]
c       + Cp_s: specific heat of the solid [J/(kg.K)]
c       + lambda_s: thermal conductivity of the solid [W/(m.K)]
c       + P: volumic power density [W/m^3]
c       + T0: initial temperature in the solid [K]
c       + Text1: temperature of the external fluid on the left side [K]
c       + Text2: temperature of the external fluid on the right side [K]
c       + Phi_rad: radiative flux absorbed on the left side [W/m^2]
c       + Trad_ext1: external radiative exchange temperature on the left side [K]
c       + Trad_ext2: external radiative exchange temperature on the right side [K]
c       + bfactor: delta_boundary=e/bfactor
c       + sfactor: delta_solid=e/sfactor
c       + Nterms: number of terms in the Fourier series expansion
c       + mc: true if MC computation has to be performed
c       + Nevent: number of statistical realizations
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c     
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      double precision box(1:Nvec_mx,1:2)
      double precision hc1
      double precision hc2
      double precision hr1
      double precision hr2
      double precision epsilon1
      double precision epsilon2
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision P
      double precision T0
      double precision Text1
      double precision Text2
      double precision Phi_rad
      double precision Trad_ext1
      double precision Trad_ext2
      double precision bfactor
      double precision sfactor
      integer Nterms
      logical mc
      integer Nevent
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Nt_mx)
      character*(Nchar_mx) infile
c     temp
      integer i,j,ios
      character*(Nchar_mx) nlines_file
      double precision x(1:Nvec_mx)
      logical is_in_box
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=data_file(1:strlen(data_file)),
     &     status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) data_file(1:strlen(data_file))
         stop
      else
         do i=1,6
            read(10,*)
         enddo ! i
         read(10,*) box(1,1)
         read(10,*) box(1,2)
         read(10,*) 
         read(10,*) box(2,1)
         read(10,*) box(2,2)
         read(10,*) 
         read(10,*) box(3,1)
         read(10,*) box(3,2)
         read(10,*)
         read(10,*) hc1
         read(10,*)
         read(10,*) hc2
         read(10,*)
         read(10,*) hr1
         read(10,*)
         read(10,*) hr2
         read(10,*)
         read(10,*) epsilon1
         read(10,*)
         read(10,*) epsilon2
         read(10,*)
         read(10,*) rho_s
         read(10,*)
         read(10,*) Cp_s
         read(10,*)
         read(10,*) lambda_s
         read(10,*)
         read(10,*) P
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) T0
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Text1
         read(10,*)
         read(10,*) Text2
         read(10,*)
         read(10,*) Phi_rad
         read(10,*)
         read(10,*) Trad_ext1
         read(10,*)
         read(10,*) Trad_ext2
         do i=1,5
            read(10,*)
         enddo                  ! i
         read(10,*) bfactor
         read(10,*)
         read(10,*) sfactor
         read(10,*)
         read(10,*) Nterms
         read(10,*)
         read(10,*) mc
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Nevent
      endif
      close(10)
c     Read probe_positions
      infile='./data/probe_positions.in'
c     
      nlines_file='./nlines'
      call get_nlines(infile,nlines_file,Nprobe)
      if (Nprobe.gt.Np_mx) then
         call error(label)
         write(*,*) 'Nprobe=',Nprobe
         write(*,*) 'while Np_mx=',Np_mx
         stop
      endif
c      
      open(11,file=infile(1:strlen(infile)),
     &     status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,Nprobe
            read(11,*) (probe_positions(i,j),j=1,dim)
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) infile(1:strlen(infile))
         stop
      endif
      close(11)
c     
c     Read time_positions
      infile='./data/time_positions.in'
c     
      nlines_file='./nlines'
      call get_nlines(infile,nlines_file,Ntime)
      if (Ntime.gt.Nt_mx) then
         call error(label)
         write(*,*) 'Ntime=',Ntime
         write(*,*) 'while Nt_mx=',Nt_mx
         stop
      endif
c     
      open(11,file=infile(1:strlen(infile)),
     &     status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,Ntime
            read(11,*) time_positions(i)
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) infile(1:strlen(infile))
         stop
      endif
      close(11)
      
c     Inconsistencies
      do i=1,dim
         if (box(i,2).le.box(i,1)) then
            call error(label)
            write(*,*) 'box(',i,',2)=',box(i,2)
            write(*,*) 'should be greater than box(',i,',1)=',box(i,1)
            stop
         endif
      enddo                     ! i
      if (hc1.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hc1=',hc1
         write(*,*) 'should be positive'
         stop
      endif
      if (hc2.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hc2=',hc2
         write(*,*) 'should be positive'
         stop
      endif
      if (hr1.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hr1=',hr1
         write(*,*) 'should be positive'
         stop
      endif
      if (hr2.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hr2=',hr2
         write(*,*) 'should be positive'
         stop
      endif
      if (epsilon1.lt.0.0D+0) then
         call error(label)
         write(*,*) 'epsilon1=',epsilon1
         write(*,*) 'should be positive'
         stop
      endif
      if (epsilon2.lt.0.0D+0) then
         call error(label)
         write(*,*) 'epsilon2=',epsilon2
         write(*,*) 'should be positive'
         stop
      endif
      if (rho_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'rho_s=',rho_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Cp_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Cp_s=',Cp_s
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_s=',lambda_s
         write(*,*) 'should be positive'
         stop
      endif
      if (T0.lt.0.0D+0) then
         call error(label)
         write(*,*) 'T0=',T0
         write(*,*) 'should be positive'
         stop
      endif
      if (Text1.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Text1=',Text1
         write(*,*) 'should be positive'
         stop
      endif
      if (Text2.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Text2=',Text2
         write(*,*) 'should be positive'
         stop
      endif
      if (Phi_rad.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Phi_rad=',Phi_rad
         write(*,*) 'should be positive'
         stop
      endif
      if (Trad_ext1.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Trad_ext1=',Trad_ext1
         write(*,*) 'should be positive'
         stop
      endif
      if (Trad_ext2.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Trad_ext2=',Trad_ext2
         write(*,*) 'should be positive'
         stop
      endif
      if (bfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'bfactor=',bfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (sfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'sfactor=',sfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (Nterms.lt.1) then
         call error(label)
         write(*,*) 'Nterms=',Nterms
         write(*,*) 'should be greater than 0'
         stop
      endif
      if (Nterms.gt.Nterms_mx) then
         call error(label)
         write(*,*) 'Nterms=',Nterms
         write(*,*) 'while Nterms_mx=',Nterms_mx
         stop
      endif
      if (Nevent.le.0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be positive'
         stop
      endif
      do i=1,Nprobe
         do j=1,dim
            x(j)=probe_positions(i,j)
         enddo
         call x_is_in_box(dim,x,is_in_box)
         if (.not.is_in_box) then
            call error(label)
            write(*,*) 'Probe position index:',i
            write(*,*) 'x=',(x(j),j=1,dim)
            write(*,*) 'is not in the box'
            stop
         endif
      enddo                     ! i
      do i=1,Ntime
         if (time_positions(i).lt.0.0D+0) then
            call error(label)
            write(*,*) 'Time index:',i
            write(*,*) 'time_positions(',i,')=',time_positions(i)
            write(*,*) '< 0'
            stop
         endif
      enddo                     ! i
            
      return
      end

