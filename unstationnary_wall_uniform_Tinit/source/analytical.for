      subroutine analytical_results(dim,Nprobe,probe_positions,
     &     Ntime,time_positions,Nterms,
     &     Tunstationary,Tinit,Tstationary)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to evaluate analytical temperature fields
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c       + Nterms: number of terms in the Fourier series expansion
c     
c     Output:
c       + Tunstationary: temperature profile at each (x/t) positions
c       + Tinit: initial temperature profile
c       + Tstationary: stationary (final) temperature profile
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Np_mx)
      integer Nterms
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     temp
      integer n
      integer itime,iprobe,iterm
      double precision time,probe_x
      double precision a(1:Nvec_mx,1:Nvec_mx)
      double precision y(1:Nvec_mx)
      double precision dbeta
      logical check,possible_root,keep_looking
      integer Nstep,Nstep_mx
      double precision max_err
      logical valid
      double precision Ts1,Ts2
      double precision x(1:Nvec_mx)
      double precision sin2betae
      double precision norm,norm_theoric
      double precision coeff(1:Nterms_mx)
      character*(Nchar_mx) str
      integer err
c     temp
      integer i,m
      logical debug
      double precision as,bs
      double precision Tb1,Tb2,Ts_analytic
      double precision e,h1,h2
      double precision int_cos,int_xcos,int_x2cos
      double precision int_sin,int_xsin,int_x2sin
      double precision phi_in,phi_out
      double precision beta,beta1,beta2
      double precision f,f1,f2
      double precision errx,errf
      logical solution_found
      double precision beta0
      integer Np
      integer Nroots
      double precision root(1:Nterms_mx)
      character*(Nchar_mx) results_file_t
      character*(Nchar_mx) results_file_x
c     functions
      double precision fbeta
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine analytical_results'
      
      Nstep_mx=1000000
      e=(box(1,2)-box(1,1))

c     Initial temperature profile
c      debug=.false.
c      n=2
c      a(1,1)=lambda_s/e+hc1+hr1
c      a(1,2)=-lambda_s/e
c      a(2,1)=-lambda_s/e
c      a(2,2)=lambda_s/e+hc2+hr2
c      y(1)=hc1*Text1+hr1*Trad_ext1+P*e/2.0D+0
c      y(2)=hc2*Text2+hr2*Trad_ext2+P*e/2.0D+0
c      check=.true.
c      max_err=1.0D-4
c      call linear_system(debug,n,a,y,check,max_err,valid,x)
      Tb10=T0
      Tb20=T0
      open(11,file='./results/Tsolid_initial.txt')
      write(11,10) 'Temperatures @ intial conditions'
      write(11,*)
      write(11,10) 'Temperature on the left boundary:'
      write(11,*) Tb10
      write(11,10) 'Temperature on the right boundary:'
      write(11,*) Tb20
      write(11,*)            
      do i=1,Nprobe
         Ts_analytic=Tb10
     &        +(Tb20-Tb10)
     &        *(probe_positions(i,1)-box(1,1))
     &        /e
         write(11,*) probe_positions(i,1),Ts_analytic
         Tinit(i)=Ts_analytic
      enddo                     ! i
      close(11)
c     Stationary temperature profile
      debug=.false.
      n=2
      a(1,1)=-lambda_s
      a(1,2)=hc1+hr1
      a(2,1)=lambda_s+e*(hc2+hr2)
      a(2,2)=hc2+hr2
      y(1)=hc1*Text1+hr1*Trad_ext1+Phi_rad
      y(2)=hc2*Text2+hr2*Trad_ext2
     &     +P*e*(1.0D+0+(hc2+hr2)*e/(2.0D+0*lambda_s))
      check=.true.
      max_err=1.0D-6
      call linear_system(debug,n,a,y,check,max_err,valid,x)
      if (.not.valid) then
         call error(label)
         write(*,*) 'Invalid result'
         write(*,*) 'while evaluating stationary conditions'
         stop
      else
         as=x(1)
         bs=x(2)
         Tb1=bs
         Tb2=-(P*e**2.0D+0)/(2.0D+0*lambda_s)+as*e+bs
         open(11,file='./results/Tsolid_stationary.txt')
         write(11,10) 'Temperatures @ stationary'
         write(11,*)
         write(11,10) 'Temperature on the left boundary:'
         write(11,*) Tb1
         write(11,10) 'Temperature on the right boundary:'
         write(11,*) Tb2
         write(11,*)        
         do i=1,Nprobe
            Ts_analytic=-(P*probe_positions(i,1)**2.0D+0)
     &           /(2.0D+0*lambda_s)
     &           +as*probe_positions(i,1)
     &           +bs
            write(11,*) probe_positions(i,1),Ts_analytic
            Tstationary(i)=Ts_analytic
         enddo                  ! i
         close(11)
      endif
c     Debug
c     Radiative flux budget
      phi_in=Phi_rad+P*e
      phi_out=hc1*(Tb1-Text1)+hr1*(Tb1-Trad_ext1)
     &     +hc2*(Tb2-Text2)+hr2*(Tb2-Trad_ext2)
      write(*,*) 'Stationnary flux budget:'
      write(*,*) 'Flux input:',phi_in
      write(*,*) 'Flux output:',phi_out
      if (dabs(phi_in-phi_out).ge.dabs(phi_in)*1.0D-4) then
         write(*,*) 'Warning: significant difference in flux !'
      endif
c     Debug

c     unstationary temperature profile

c     implicit equation of beta
      h1=(hc1+hr1)/lambda_s
      h2=(hc2+hr2)/lambda_s
c     Solving the unstationary homogeneous problem
      m=1
      beta1=0.0D+0
      dbeta=1.0D-2
      f1=fbeta(e,h1,h2,beta1,m)
      errx=1.0D-7
      errf=1.0D-9
      Nroots=0
      Nstep=0
      keep_looking=.true.
      do while (keep_looking)
         beta2=beta1+dbeta
         Nstep=Nstep+1
         f2=fbeta(e,h1,h2,beta2,m)
c     disriminate possible roots
         possible_root=.false.
         if (f1*f2.lt.0.0D+0) then
            if (dabs(f1*f2).lt.1.0D+4) then
               possible_root=.true.
            endif
         endif
c     
         if (possible_root) then
            call solve_fbeta_root(e,h1,h2,m,beta1,beta2,errx,errf,
     &           solution_found,beta0)
            if (solution_found) then
               Nroots=Nroots+1
               if (Nroots.gt.Nterms_mx) then
                  call error(label)
                  write(*,*) 'Nroots=',Nroots
                  write(*,*) '> Nterms_mx=',Nterms_mx
                  stop
               else
                  root(Nroots)=beta0
                  Nstep=0
c     Stop when enough terms have been found
                  if (Nroots.ge.Nterms) then
                     keep_looking=.false.
                  endif
               endif
            else
               write(*,*) 'no solution found in interval: [',
     &              beta1,'-',beta2,']'
            endif               ! solution_found
         endif                  ! possible_root
         beta1=beta2
         f1=f2
         if (Nstep.gt.Nstep_mx) then
            beta1=root(Nroots)
            f1=fbeta(e,h1,h2,beta1,m)
            dbeta=dbeta/1.0D+1
            Nstep=0
            if (dbeta.lt.1.0D-20) then ! fail
               keep_looking=.false.
            endif
         endif
      enddo                     ! while (keep_looking)
c
c     Generate log
      open(11,file='./results/roots_log.txt')
      write(11,*) 'Number of roots found:',Nroots
      if (Nroots.gt.0) then
         do i=1,Nroots
            write(11,*) 'root(',i,')=',root(i),fbeta(e,h1,h2,root(i),m)
            norm_theoric=0.50D+0*((root(i)**2.0D+0+h1**2.0D+0)
     &           *(e+h2/(root(i)**2.0D+0+h2**2.0D+0))+h1)
            sin2betae=(root(i)*(h1+h2))**2.0D+0/
     &           (root(i)**2.0D+0
     &           *(root(i)**2.0D+0+h1**2.0D+0+h2**2.0D+0)
     &           +(h1*h2)**2.0D+0)
            norm=e*(root(i)**2.0D+0+h1**2.0D+0)/2.0D+0
     &           +(0.50D+0*(root(i)**2.0D+0-h1**2.0D+0)/root(i)
     &           *(root(i)**2.0D+0-h1*h2)/(root(i)*(h1+h2))
     &           +h1)*sin2betae
            write(11,*) 'norm_theoric=',norm_theoric,' norm=',norm
         enddo                  ! i
      endif
      close(11)
c     
c     Evaluation of the Fourier series coefficients
      do i=1,Nroots
         norm=0.50D+0*((root(i)**2.0D+0+h1**2.0D+0)
     &        *(e+h2/(root(i)**2.0D+0+h2**2.0D+0))+h1)
         int_cos=dsin(root(i)*e)/root(i)
         int_sin=(1.0D+0-dcos(root(i)*e))/root(i)
         int_xcos=e*dsin(root(i)*e)/root(i)-int_sin/root(i)
         int_xsin=-e*dcos(root(i)*e)/root(i)+int_cos/root(i)
         int_x2cos=(e**2.0D+0)*dsin(root(i)*e)/root(i)
     &        -2.0D+0*int_xsin/root(i)
         int_x2sin=-(e**2.0D+0)*dcos(root(i)*e)/root(i)
     &        +2.0D+0*int_xcos/root(i)
         coeff(i)=(
     &        (Tb10-bs)*(root(i)*int_cos+h1*int_sin)
     &        +((Tb20-Tb10)/e-as)*(root(i)*int_xcos+h1*int_xsin)
     &        +P/(2.0D+0*lambda_s)*(root(i)*int_x2cos+h1*int_x2sin)
     &        )/norm
      enddo                     ! i
c     Generate log
      open(11,file='./results/coefficients_log.txt')
      do i=1,Nroots
         write(11,*) coeff(i)
      enddo                     ! i
      close(11)
c
      do itime=1,Ntime
c     write(*,*) 'time index: ',itime,' /',Ntime
         time=time_positions(itime)
         do iprobe=1,Nprobe
            probe_x=probe_positions(iprobe,1)
c     Fourier series evaluation
            Ts2=0.0D+0
            do iterm=1,Nroots
               Ts2=Ts2
     &              +coeff(iterm) ! Fourier coefficient
     &              *(root(iterm)*dcos(root(iterm)*probe_x)
     &              +h1*dsin(root(iterm)*probe_x)) ! function of space
     &              *dexp(-lambda_s/(rho_s*Cp_s)
     &              *(root(iterm)**2.0D+0)*time) ! function of time
            enddo               ! iterm
            Ts_analytic=Tstationary(iprobe)+Ts2
c     --------------------------------------------------------------------------------------
c     Record results:
c     + time files
            call num2str(itime,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'itime=',itime
               stop
            else
               results_file_t='./results/Tsolid_analytical_t'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     + space files
            call num2str(iprobe,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'iprobe=',iprobe
               stop
            else
               results_file_t='./results/Tsolid_analytical_x'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     --------------------------------------------------------------------------------------
         enddo                  ! iprobe
         close(11)
      enddo                     ! itime
      
      return
      end



      subroutine solve_fbeta_root(e,h1,h2,m,beta1,beta2,errx,errf,
     &     solution_found,beta0)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a root of f(beta)=0 between two known boundaries
c     
c     Input:
c       + e: wall thickness [m]
c       + h1: total exchange coefficient of the left
c       + h2: total exchange coefficient of the right
c       + m: index of the period
c       + beta1: lower beta boundary
c       + beta2: lower beta boundary
c       + errx: maximum error over x (beta)
c       + errf: maximum error over f
c    
c    Output:
c      + solution_found: true if a solution has been found
c      + beta0: f(beta0)=0
c    
c    I/O
      double precision e,h1,h2
      integer m
      double precision beta1,beta2
      double precision errx,errf
      logical solution_found
      double precision beta0
c     temp
      double precision beta_mid
      double precision f1,f2,f_mid
      integer iter
      logical keep_running
c     functions
      double precision fbeta
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine solve_fbeta_root'

      f1=fbeta(e,h1,h2,beta1,m)
      f2=fbeta(e,h1,h2,beta2,m)
      keep_running=.true.
      iter=0
      do while (keep_running)
         iter=iter+1
         beta_mid=(beta1+beta2)/2.0D+0
         f_mid=fbeta(e,h1,h2,beta_mid,m)
         if (f1*f_mid.lt.0.0D+0) then
            beta2=beta_mid
            f2=f_mid
         else if (f_mid*f2.lt.0.0D+0) then
            beta1=beta_mid
            f1=f_mid
         else
            call error(label)
            write(*,*) 'beta1=',beta1,' f1=',f1
            write(*,*) 'beta2=',beta2,' f2=',f2
            write(*,*) 'beta_mid=',beta_mid,' f_mid=',f_mid
            write(*,*) 'f1*f_mid=',f1*f_mid
            write(*,*) 'f2*f_mid=',f2*f_mid
            write(*,*) 'one should be negative'
            stop
         endif
c     Debug
c         write(*,*) iter,beta1,beta2,dabs(beta2-beta1),dabs(f2-f1)
c     Debug
         if (dabs(beta2-beta1).lt.errx) then
            solution_found=.true.
            beta0=beta_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'beta2-beta1=',dabs(beta2-beta1),beta0
c     Debug
         endif
         if (dabs(f2-f1).lt.errf) then
            solution_found=.true.
            beta0=beta_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'f2-f1=',dabs(f2-f1),beta0
c     Debug
         endif
         if (iter.gt.Niter_mx) then
            solution_found=.false.
            keep_running=.false.
         endif
      enddo ! while (keep_running)

      return
      end



      double precision function fbeta(e,h1,h2,beta,m)
      implicit none
      include 'param.inc'
c      
c     Purpose: to evaluate the function of beta which roots
c     have to be found
c     
c     Input:
c       + e: wall thickness [m]
c       + h1: total exchange coefficient of the left
c       + h2: total exchange coefficient of the right
c       + beta: value of beta where the function has to be evaluated
c       + m: index of the period
c    
c     Output:
c       + fbeta: value of the function for beta
c     
c     I/O
      double precision e,h1,h2,beta
      integer m
      
      fbeta=dtan(2.0D+0*pi*(m-1)+beta*e)
     &     -beta*(h1+h2)/(beta**2.0D+0-h1*h2)
      
      return
      end
      
