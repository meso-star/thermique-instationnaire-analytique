      subroutine min_box_dimension(dim)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to identify the smallest dimension of the box
c     
c     Input:
c       + dim: dimension of space
c     
c     Output:
c       + emin: minimum dimension of the box (in com_data.inc)
c     
c     I/O
      integer dim
c     temp
      integer i
      double precision dmin
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine min_box_dimension'

      dmin=box(1,2)-box(1,1)
      do i=2,3
         if ((box(i,2)-box(i,1)).lt.dmin) then
            dmin=box(i,2)-box(i,1)
         endif
      enddo                     ! i
      emin=dmin

      return
      end
      

      subroutine x_is_in_box(dim,x,is_in_box)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to check whether of not a position is in the box
c     
c     Input:
c       + dim: dimension of space
c       + x: position to check
c     
c     Output:
c       + is_in_box: true if "x" is in the box; false otherwise
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      logical is_in_box
c     temp
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine x_is_in_box'

      is_in_box=.true.
      do i=1,dim
c     intersection with plane x=box(i,1)
         if ((x(i).lt.box(i,1)).or.(x(i).gt.box(i,2))) then
            is_in_box=.false.
            goto 666
         endif
      enddo                     ! i
 666  continue
      
      return
      end
      

      subroutine x_is_on_boundary(dim,x,is_on_boundary,boundary_index)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to check whether of not a position is over the boundary of the box
c     
c     Input:
c       + dim: dimension of space
c       + x: position to check
c     
c     Output:
c       + is_on_boundary: true if "x" is on the boundary; false otherwise
c       + boundary_index: index of boundary if is_on_boundary=T
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      logical is_on_boundary
      integer boundary_index
c     temp
      integer i,j
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-8)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine x_is_on_boundary'

      is_on_boundary=.false.
      do i=1,dim
         do j=1,2
            if (dabs(x(i)-box(i,j)).lt.epsilon) then
               is_on_boundary=.true.
               boundary_index=2*(i-1)+j
               goto 666
            endif
         enddo                  ! j
      enddo                     ! i
 666  continue
      
      return
      end
      
