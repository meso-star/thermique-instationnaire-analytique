      program main
      implicit none
      include 'mpif.h'
      include 'max.inc'
      include 'com_data.inc'
      include 'formats.inc'
c     
c     Purpose: to compute a temperature using a statistical algorithm
c
c     MPICH
      integer ierr,errorcode,np,pindex,proc
      integer stat(MPI_STATUS_SIZE)
      logical eoc,fpf
      logical free(1:Nproc_mx)
      logical stopped(1:Nproc_mx)
      integer code0,codep
c     Variables
      integer dim,seed
      character*(Nchar_mx) data_file
      character*(Nchar_mx) results_file_t
      character*(Nchar_mx) results_file_x
      double precision probe_time
      integer Nprobe,Ntime
      integer Nterms
      logical mc
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      double precision time_positions(1:Nt_mx)
      integer type
      double precision probe_x(1:Nvec_mx)
      integer boundary_index
      logical debug
      double precision avg,variance,std_dev
      double precision T,DeltaTphi,DeltaTp,contrib,sum,sum2
      double precision Ts(1:Nt_mx)
      double precision delta_Ts(1:Nt_mx)
      double precision r
      integer itime
c     analytical results
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     progress display
      integer ndone,Nevent_tot
      double precision fdone
      character*(Nchar_mx) str
      character*(Nchar_mx) fmt
      integer err
c     temp
      integer len,i,j,iprobe,event,nsp
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-8)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='program main'

c     MPICH
      call MPI_INIT(ierr)
      if (ierr.NE.MPI_SUCCESS) THEN
         call error(label)
         write(*,*) 'MPI could not be initialized'
         call MPI_ABORT(MPI_COMM_WORLD,errorcode,ierr)
      endif
c     Get 'np', the number of processes
      call MPI_COMM_SIZE(MPI_COMM_WORLD,np,ierr)
      if (np.lt.2) then
         call error(label)
         write(*,*) 'Number of processes is:',np
         write(*,*) 'and it should be greater than 1'
         stop
      endif
      if (np.gt.Nproc_mx) then
         call error(label)
         write(*,*) 'Number of processes is:',np
         write(*,*) 'while Nproc_mx=',Nproc_mx
         stop
      endif
c     Get 'pindex', the index of the current process
      call MPI_COMM_RANK(MPI_COMM_WORLD,pindex,ierr)
c     MPICH
c     
      if (pindex.eq.0) then 
         dim=3
         ndone=0
         len=6
         call num2str(len,str,err)
         if (err.eq.0) then
            fmt='(i'//str(1:strlen(str))//',a)'
         endif
c     Initialization:
c     + read random seed
         call read_seed(seed)
c     + Initialization of the random number generator
         call zufalli(seed)
c     + read input data (configuration, external conditions...)
         data_file='./data.in'
         call read_data(data_file,dim,box,
     &        hc1,hc2,hr1,hr2,epsilon1,epsilon2,
     &        rho_s,Cp_s,lambda_s,P,T0,
     &        Text1,Text2,Phi_rad,Trad_ext1,Trad_ext2,
     &        bfactor,sfactor,Nterms,mc,
     &        Nevent,Nprobe,probe_positions,Ntime,time_positions)
c     + remove previous result files
         call init(mc)
c     + more initialization
         call min_box_dimension(dim)
         if (Ntime.gt.Nt_mx) then
            call error(label)
            write(*,*) 'Ntime=',Ntime
            write(*,*) 'while Nt_mx=',Nt_mx
            stop
         endif
         Nevent_tot=Nevent*Nprobe*Ntime

         write(*,*) 'Analytical computation...'
         call analytical_results(dim,Nprobe,probe_positions,
     &        Ntime,time_positions,Nterms,
     &        Tunstationary,Tinit,Tstationary)
         if (mc) then
            write(*,*) 'Monte-Carlo integration...'
            write(*,"(a)",advance='no') 'Done:     0 %'
c     
            do itime=1,Ntime
c     Debug
c               write(*,*) 'time index: ',itime,' /',Ntime
c     Debug
               probe_time=time_positions(itime)
               debug=.false.
               do iprobe=1,Nprobe
                  do j=1,dim
                     probe_x(j)=probe_positions(iprobe,j)
                  enddo         ! j
c     Debug
c                  if (debug) then
c                  write(*,*) 'iprobe=',iprobe,' /',Nprobe
c                  endif
c     Debug
                  if (dabs(probe_x(1)-box(1,1)).le.epsilon) then
c     Temperature at a boundary: type=1, and needs the index of the boundary
                     type=1
                     boundary_index=1
                  else if (dabs(probe_x(1)-box(1,2)).le.epsilon) then
c     Temperature at a boundary: type=1, and needs the index of the boundary
                     type=1
                     boundary_index=2
                  else
c     Temperature in the solid: type=2
                     type=2
                  endif         ! j
c     initialization of sums
                  sum=0.0D+0
                  sum2=0.0D+0
c     sending common data to all processes
                  code0=1
                  do proc=1,np-1
                     call MPI_SEND(code0,1,MPI_INTEGER
     &                    ,proc,1,MPI_COMM_WORLD,ierr)
                  enddo         ! proc
                  do proc=1,np-1
                     call random_gen(r)
                     seeds(proc)=int(10000*r)
                  enddo         ! proc
                  call send_global_data_mpibcast(np,dim)
c     MC loop
                  do proc=1,np-1
                     free(proc)=.true.
                     stopped(proc)=.false.
                  enddo
                  eoc=.false.
                  event=0
                  do while (.not.eoc)
 444                 continue
                     call find_free_process(free,np-1,fpf,proc)
                     if (fpf) then ! a free child process found
                        event=event+1
                        if (event.le.Nevent) then
c     Debug
c                           if (debug) then
c                              write(*,*) 'event=',event
c                           endif
c     Debug
                           code0=2
                           call MPI_SEND(code0,1,MPI_INTEGER
     &                          ,proc,1,MPI_COMM_WORLD,ierr)
                           call MPI_SEND(debug,1,MPI_LOGICAL
     &                          ,proc,1,MPI_COMM_WORLD,ierr)
                           call MPI_SEND(dim,1,MPI_INTEGER
     &                          ,proc,1,MPI_COMM_WORLD,ierr)
                           call MPI_SEND(type,1,MPI_INTEGER
     &                          ,proc,1,MPI_COMM_WORLD,ierr)
                           call MPI_SEND(boundary_index,1,MPI_INTEGER
     &                          ,proc,1,MPI_COMM_WORLD,ierr)
                           call MPI_SEND(probe_time,1,
     &                          MPI_DOUBLE_PRECISION,proc,1,
     &                          MPI_COMM_WORLD,ierr)
                           call MPI_SEND(probe_x,dim,
     &                          MPI_DOUBLE_PRECISION,proc,1,
     &                          MPI_COMM_WORLD,ierr)
                           free(proc)=.false. ! child process index 'proc' is busy
                        else    ! chunk>nchunks or Nkrun(chunk)=0
                           free(proc)=.false. ! child process index 'proc' is busy
                           stopped(proc)=.true. ! means child process index 'proc' has been stopped
                        endif
                        goto 444
                     else       ! no free child process found
                        call stopped_processes(stopped,np-1,nsp)
                        if (nsp.eq.np-1) then
                           eoc=.true.
                           goto 111
                        endif
c     wait for results
                        call MPI_RECV(proc,1,MPI_INTEGER
     &                       ,MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                       MPI_COMM_WORLD,stat,ierr)
                        call MPI_RECV(contrib,1,MPI_DOUBLE_PRECISION
     &                       ,proc,MPI_ANY_TAG,
     &                       MPI_COMM_WORLD,stat,ierr)
c     print status
                        ndone=ndone+1
                        fdone=dble(ndone)/dble(Nevent_tot)*1.0D+2
                        if (fdone.eq.dble(floor(fdone))) then
                           do j=1,len+2
                              write(*,"(a)",advance='no') "\b"
                           enddo ! j
                           write(*,fmt(1:strlen(fmt)),advance='no')
     &                          floor(fdone),' %'
                        endif
c     update sums
                        sum=sum+contrib
                        sum2=sum2+contrib**2.0D+0
c     release the process
                        free(proc)=.true. ! child process index 'proc' is free again
c     
                     endif      ! fpf or not
 111                 continue
                  enddo         ! while eoc=.false.
c     
c     get the average and standard deviation
                  call statistics(Nevent,sum,sum2,avg,variance,std_dev)
                  Ts(itime)=avg
                  delta_Ts(itime)=std_dev
c     --------------------------------------------------------------------------------------
c     Record results:
c     + time files
                  call num2str(itime,str,err)
                  if (err.eq.1) then
                     call error(label)
                     write(*,*) 'Could not convert to string:'
                     write(*,*) 'itime=',itime
                     stop
                  else
                     results_file_t='./results/Tsolid_t'
     &                    //str(1:strlen(str))
     &                    //'.txt'
                  endif
                  open(11,file=results_file_t(1:strlen(results_file_t))
     &                 ,access='append')
                  write(11,*) (probe_x(i),i=1,dim),probe_time,
     &                 Ts(itime),delta_Ts(itime)
                  close(11)
c     + space files
                  call num2str(iprobe,str,err)
                  if (err.eq.1) then
                     call error(label)
                     write(*,*) 'Could not convert to string:'
                     write(*,*) 'iprobe=',iprobe
                     stop
                  else
                     results_file_t='./results/Tsolid_x'
     &                    //str(1:strlen(str))
     &                    //'.txt'
                  endif
                  open(11,file=results_file_t(1:strlen(results_file_t))
     &                 ,access='append')
                  write(11,*) (probe_x(i),i=1,dim),probe_time,
     &                 Ts(itime),delta_Ts(itime)
                  close(11)
c     --------------------------------------------------------------------------------------
               enddo            ! iprobe
            enddo               ! itime
         endif                  ! mc
c     
c     write(*,*)
         
c     Before quitting: generate a new random seed
         call write_seed()
c     stopping all child processes
         do proc=1,np-1
            code0=0
            call MPI_SEND(code0,1,MPI_INTEGER
     &           ,proc,1,MPI_COMM_WORLD,ierr)
         enddo
         write(*,*) '...done'
c     
         else                   ! every other process
c     
 333        continue
c     wait for "codep" from root process
            call MPI_RECV(codep,1,MPI_INTEGER
     &           ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
c     codep=0 means exit
            if (codep.eq.0) then
               goto 666
c     codep=1 means common data reception
            else if (codep.eq.1) then
               call receive_global_data_mpibcast(np,dim)
c     initialization of the random number generator
            call zufalli(seeds(pindex))
c     codep=2 means data reception and computation
            else if (codep.eq.2) then
               call MPI_RECV(debug,1,MPI_LOGICAL
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(dim,1,MPI_INTEGER
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(type,1,MPI_INTEGER
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(boundary_index,1,MPI_INTEGER
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(probe_time,1,MPI_DOUBLE_PRECISION
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(probe_x,dim,MPI_DOUBLE_PRECISION
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
c     computation
               call get_temperature(debug,dim,type,boundary_index,
     &              probe_x,probe_time,T,DeltaTphi,DeltaTp)
c     send results to root process
               call MPI_SEND(pindex,1,MPI_INTEGER
     &              ,0,1,MPI_COMM_WORLD,ierr)
               call MPI_SEND(T+DeltaTphi+DeltaTp,1,
     &              MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
            endif               ! codep
c     
c     wait for a new execution code
            goto 333
c     
 666        continue
c     
      endif                     ! pindex
c     MPICH
      call MPI_FINALIZE(ierr)
c     MPICH

      end
      
