Calcul du profil de température spatio/temporel dans une boule de rayon R:

- conditions aux limites: convection + rayonnement en paroi

- condition initiale: profil uniforme
