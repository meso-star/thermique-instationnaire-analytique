      subroutine get_temperature(debug,dim,type,
     &     boundary_index,probe_x,probe_time,T,DeltaTp)
      implicit none
      include 'mpif.h'
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to make a single MC realization of the temperature at a
c     given position (fluid, boundary, solid) and at a given time
c     
c     Input:
c       + debug: true if debug mode
c       + dim: dimension of space
c       + type: temperature type
c         type=1: temperature at a boundary (defined by boundary_type)
c         type=2: temperature at a given position (defined by probe_x) in the solid
c       + boundary_index: index of the boundary if type=1
c         - boundary_index=1: x=x_min
c         - boundary_index=2: x=x_max
c         - boundary_index=3: y=y_min
c         - boundary_index=4: y=y_max
c         - boundary_index=5: z=z_min
c         - boundary_index=6: z=z_max
c       + probe_x: value of (x,y,z) [m] if type=2
c       + probe_time: value of time [s]
c     
c     Output:
c       + T: realization of the temperature [K]
c       + DeltaTp: temperature increase due to volumic power [K]
c     
c     I/O
      logical debug
      integer dim
      integer type
      integer boundary_index
      double precision probe_x(1:Nvec_mx)
      double precision probe_time
      double precision T
      double precision DeltaTp
c     temp
      double precision time,x(1:Nvec_mx)
      integer bindex
      double precision delta_boundary
      double precision delta_solid
      double precision normal(1:Nvec_mx)
      double precision inv_normal(1:Nvec_mx)
      double precision deltax(1:Nvec_mx)
      logical over
      integer i,j
      double precision rn
      double precision p1,p2
      logical is_inside,is_on_boundary
      double precision hconv,hrad,emissivity
      logical intersection
      logical intersection1,intersection2
      integer boundary_index1,boundary_index2
      double precision d2b,d2b1,d2b2
      double precision u(1:Nvec_mx)
      double precision minus_u(1:Nvec_mx)
      double precision v(1:Nvec_mx)
      double precision x_init(1:Nvec_mx)
      double precision x1(1:Nvec_mx)
      double precision x2(1:Nvec_mx)
      double precision x_int1(1:Nvec_mx)
      double precision x_int2(1:Nvec_mx)
      double precision alpha
      double precision delta_x,dmin
      double precision delta_t
      logical keep_running
      logical int_found
      double precision dist2bound
      double precision x_int(1:Nvec_mx)
      double precision d,theta,phi
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine get_temperature'

      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
      endif
      
      time=probe_time
      call copy_vector(dim,probe_x,x)
      DeltaTp=0.0D+0
      over=.false.

      do while (.not.over)
c     ------------------------------------------------------------------------------
         if (type.eq.1) then    ! temperature at a boundary
            if (time.le.0.0D+0) then
               if (boundary_index.eq.1) then
                  call initial_solid_temperature(dim,x,T)
               endif
               over=.true.
               goto 666
            else
               delta_boundary=R/bfactor
               call exchange_coefficients(dim,boundary_index,
     &              hconv,hrad,emissivity)
c     p1 is the probability to evaluate the temperature of the fluid
               p1=hconv/(hconv+hrad*emissivity+lambda_s/delta_boundary)
c     p2 is the probability to evaluate the radiative temperature
               p2=hrad*emissivity/
     &              (hconv+hrad*emissivity+lambda_s/delta_boundary)
c     
               call random_gen(rn)
               if (rn.le.p1) then
c     Temperature of the fluid
                  call Tfluid(debug,dim,time,boundary_index,T)
                  over=.true.
                  goto 666
               else if (rn.le.p1+p2) then
c     Radiative temperature
                  call Tradia(debug,dim,time,boundary_index,T)
                  over=.true.
                  goto 666
               else
c     Conductive branch
                  call normal_on_sphere(dim,x,normal)
                  call scalar_vector(dim,-1.0D+0,normal,inv_normal)
                  if (boundary_index.eq.1) then
                     call scalar_vector(dim,delta_boundary,inv_normal,
     &                    deltax)
                     call add_vectors(dim,x,deltax,x)
                  else
                     call error(label)
                     write(*,*) 'boundary_index=',boundary_index
                     write(*,*) 'allowed values: 1'
                     stop
                  endif
c     check
                  call x_is_in_volume(dim,x,is_inside)
                  if (.not.is_inside) then
                     call error(label)
                     write(*,*) 'x=',(x(i),i=1,dim)
                     write(*,*) 'is not in the volume'
                     stop
                  endif
c     check
                  type=2
               endif
            endif               ! time<0 or >0
c     ------------------------------------------------------------------------------
         else if (type.eq.2) then ! temperature in the solid
            delta_solid=R/sfactor
            keep_running=.true.
c     
            do while (keep_running)
               delta_x=delta_solid
c     - sample propagation direction
               call sample_uniform_sphere(u)
               call scalar_vector(dim,-1.0D+0,u,minus_u)
c     - look for intersections from x_init along +u and -u
               call copy_vector(dim,x,x_init)
               call scalar_vector(dim,delta_x,u,v)
               call add_vectors(dim,x_init,v,x1) ! x1=x_init+u*delta_x
               call scalar_vector(dim,delta_x,minus_u,v)
               call add_vectors(dim,x_init,v,x2) ! x2=x_init-u*delta_x
c     -------------------------------------------------------------------------
c     Intersections
               call intersect(debug,dim,x_init,u,delta_x,x1,
     &              intersection1,boundary_index1,d2b1,x_int1)
               call intersect(debug,dim,x_init,minus_u,delta_x,x2,
     &              intersection2,boundary_index2,d2b2,x_int2)
               if (intersection1) then
c     check
                  call x_is_in_volume(dim,x_int1,is_inside)
                  if (.not.is_inside) then
                     call error(label)
                     write(*,*) 'x_int1=',(x_int1(i),i=1,dim)
                     write(*,*) 'is not in the volume'
                     write(*,*) 'intersection1=',intersection1
                     if (intersection1) then
                        write(*,*) 'boundary_index1=',boundary_index1
                     endif
                     write(*,*) 'x_init=',(x_init(i),i=1,dim)
                     write(*,*) 'x1=',(x1(i),i=1,dim)
                     stop
                  endif
c     check
                  if (d2b1.lt.delta_x) then
                     delta_x=d2b1
                  endif
               endif            ! intersection1
               if (intersection2) then
c     check
                  call x_is_in_volume(dim,x_int2,is_inside)
                  if (.not.is_inside) then
                     call error(label)
                     write(*,*) 'x_int2=',(x_int2(i),i=1,dim)
                     write(*,*) 'is not in the volume'
                     write(*,*) 'intersection2=',intersection2
                     if (intersection2) then
                        write(*,*) 'boundary_index2=',boundary_index2
                     endif
                     write(*,*) 'x_init=',(x_init(i),i=1,dim)
                     write(*,*) 'x2=',(x2(i),i=1,dim)
                     stop
                  endif
c     check
                  if (d2b2.lt.delta_x) then
                     delta_x=d2b2
                  endif
               endif            ! intersection2
c     
c     Sample a time interval according to the relevant time constant "alpha"
               alpha=6.0D+0*lambda_s/(rho_s*Cp_s*delta_x**2.0D+0)
               call sample_exp(alpha,delta_t)
c     Go back in time
               time=time-delta_t
               if (debug) then
                  write(*,*) 'x=',x,' delta_x=',delta_x,' time=',time
               endif
c     
               if (time.le.0.0D+0) then
c     If time<0, then return the initial temperature of the solid
                  call initial_solid_temperature(dim,x,T)
                  keep_running=.false.
                  over=.true.
                  if (debug) then
                     write(*,*) 'Exit because time=',time
                  endif
                  goto 666
               endif            ! if time>0 or <0
c     
c     Sample +u or -u
               call random_gen(rn)
               if (rn.le.0.50D+0) then ! choose +u
                  call scalar_vector(dim,delta_x,u,v)
                  call add_vectors(dim,x_init,v,x) ! x=x_init+u*delta_x
               else             ! choose -u
                  call scalar_vector(dim,delta_x,minus_u,v)
                  call add_vectors(dim,x_init,v,x) ! x=x_init-u*delta_x
               endif
               DeltaTp=DeltaTp+P*(delta_x**2.0D+0)/(6.0D+0*lambda_s)
               call x_is_on_boundary(dim,x,is_on_boundary,bindex)
               if (is_on_boundary) then
                  type=1
                  boundary_index=bindex
                  keep_running=.false.
                  goto 111
               endif
c     
c     check
               call x_is_in_volume(dim,x,is_inside)
               if (.not.is_inside) then
                  call error(label)
                  write(*,*) 'x=',(x(i),i=1,dim)
                  write(*,*) 'is not in the volume'
                  call cart2spher(x(1),x(2),x(3),d,theta,phi)
                  write(*,*) 'd=',d
                  write(*,*) 'intersection1=',intersection1
                  write(*,*) 'intersection2=',intersection2
                  write(*,*) 'x_init=',(x_init(j),j=1,dim)
                  call cart2spher(x_init(1),x_init(2),x_init(3),
     &                 d,theta,phi)
                  write(*,*) 'd_init=',d
                  call scalar_vector(dim,delta_solid,u,v)
                  call add_vectors(dim,x_init,v,x1) ! x1=x_init+u*delta_x
                  write(*,*) 'x1=',(x1(i),i=1,dim)
                  call cart2spher(x1(1),x1(2),x1(3),d,theta,phi)
                  write(*,*) 'd1=',d
                  call scalar_vector(dim,delta_solid,minus_u,v)
                  call add_vectors(dim,x_init,v,x2) ! x1=x_init-u*delta_x
                  write(*,*) 'x2=',(x2(i),i=1,dim)
                  call cart2spher(x2(1),x2(2),x2(3),d,theta,phi)
                  write(*,*) 'd2=',d
                  call intersection_with_sphere(.true.,dim,
     &                 x_init,minus_u,delta_solid,x2,R,
     &                 intersection,d2b,x)
                  write(*,*) 'intersection=',intersection
                  write(*,*) 'x=',(x(i),i=1,dim)
                  call cart2spher(x(1),x(2),x(3),d,theta,phi)
                  write(*,*) 'd=',d
                  stop
               endif
c     check
c
 111           continue
c
            enddo               ! keep_running
c     ------------------------------------------------------------------------------
         else
            call error(label)
            write(*,*) 'type=',type
            stop
         endif
 666     continue
      enddo                     ! over
c      
      return
      end
      
