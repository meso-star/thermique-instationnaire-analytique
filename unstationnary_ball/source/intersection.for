      subroutine intersect(debug,dim,x_init,u,delta,x1,
     &     intersection,boundary_index,d2b,x)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to intersect a ray with a sphere between x_init and x1
c     
c     Input:
c       + dim: dimension of space
c       + x_init: initial position
c       + u: propagation direction
c       + delta: displacement
c       + x1: final position
c     
c     Output:
c       + intersection: true if intersection has been found
c       + boundary_index: index of the boundary
c         - boundary_index=1: sphere
c       + d2b: distance to boundary
c       + x: final position
c         - is equal to x1 if no intersection has been found
c         - is equal to the intersection position otherwise
c     
c     I/O
      logical debug
      integer dim
      double precision x_init(1:Nvec_mx)
      double precision u(1:Nvec_mx)
      double precision delta
      double precision x1(1:Nvec_mx)
      logical intersection
      integer boundary_index
      double precision d2b
      double precision x(1:Nvec_mx)
c     temp
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine intersect'
      
      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
         write(*,*) 'x_init=',x_init
         write(*,*) 'u=',u
         write(*,*) 'delta=',delta
         write(*,*) 'x1=',x1
      endif

      call forward_intersections(debug,dim,x_init,u,delta,x1,
     &     int_found,d2int,x_list)      
c
c     Check that the distance to the
c     intersection is smaller than displacement 'delta'
c     
      call filter_intersections_by_distance(debug,dim,
     &     int_found,d2int,x_list,delta)
c
c     Identify the closest intersection
c
      call find_closest_intersection(dim,debug,
     &     int_found,d2int,x_list,
     &     intersection,boundary_index,d2b,x)
      if (.not.intersection) then
         call copy_vector(dim,x1,x)
      endif

      return
      end



      subroutine forward_intersections(debug,dim,x_init,u,delta,x1,
     &     int_found,d2int,x_list)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c
c     Purpose: to find the intersection between a ray and a radius=R sphere.
c     A solution is returned in the case when the intersection is in
c     the forward direction only.
c
c     Input:
c       + dim: dimension of space
c       + x_init: initial position
c       + u: propagation direction
c       + delta: displacement
c       + x1: final position
c     
c     Output:
c       + int_found: true if intersection has been found (for every surface)
c       + d2int: distance to boundary (for every surface)
c       + x_list: intersection position (for every surface)
c     
c     I/O
      logical debug
      integer dim
      double precision x_init(1:Nvec_mx)
      double precision u(1:Nvec_mx)
      double precision delta
      double precision x1(1:Nvec_mx)
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
c     temp
      integer i,j,imin
      double precision x_tmp(1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine forward_intersections'
c
      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
         write(*,*) 'x_init=',x_init,' delta=',delta
      endif

c     Intersection with radius=R sphere
      call intersection_with_sphere(debug,dim,
     &     x_init,u,delta,x1,R,
     &     int_found(1),d2int(1),x_tmp)
      do j=1,dim
         x_list(1,j)=x_tmp(j)
      enddo                     ! j
c     
      return
      end



      subroutine filter_intersections_by_distance(debug,dim,
     &     int_found,d2int,x_list,delta)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to filter the intersection list with respect to the displacement 'delta'
c     
c     Intput:
c       + int_found: true if intersection has been found (for every surface)
c       + d2int: distance to boundary (for every surface)
c       + x_list: intersection position (for every surface)
c       + delta: displacement
c     
c     Output: (int_found, d2int, x_list) updated
c     
c     I/O
      logical debug
      integer dim
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
      double precision delta
c     temp
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine filter_intersections_by_zones'

      do i=1,1
         if (int_found(i)) then ! intersection with sphere
            if (d2int(i).gt.delta) then
               int_found(i)=.false.
            endif
         endif
      enddo                     ! i
      
      return
      end      



      subroutine find_closest_intersection(dim,debug,
     &     int_found,d2int,x_list,
     &     intersection,boundary_index,d2b,x)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to identify the closest intersection from the list
c     
c     Intput:
c       + int_found: true if intersection has been found (for every surface)
c       + d2int: distance to boundary (for every surface)
c       + x_list: intersection position (for every surface)
c     
c     Output:
c       + intersection: true if a intersection has been found
c       + boundary_index: index of the boundary where closest intersection is located
c       + d2b: distance to the closest intersection
c       + x: position of the closest intersection
c
c     I/O
      logical debug
      integer dim
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
      logical intersection
      integer boundary_index
      double precision d2b
      double precision x(1:Nvec_mx)
c     temp
      integer i,j,imin
      logical one_int_found,imin_found
      double precision dmin,dmax
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine find_closest_intersection'
      
      dmax=0.0D+0
      one_int_found=.false.
      do i=1,1
         if (int_found(i)) then
            if (d2int(i).ge.dmax) then
               dmax=d2int(i)
               one_int_found=.true.
            endif
         endif
      enddo                     ! i
c     check
      if ((one_int_found).and.(dmax.lt.0.0D+0)) then
         call error(label)
         write(*,*) 'dmax=',dmax
         write(*,*) '< 0'
         stop
      endif
c     check
      imin_found=.false.
      if (one_int_found) then
         dmin=dmax
         do i=1,1
            if (int_found(i)) then
               if ((d2int(i).ge.0.0D+0).and.(d2int(i).le.dmin)) then
                  dmin=d2int(i)
                  imin=i
                  imin_found=.true.
               endif
            endif
         enddo                  ! i
      endif                     ! one_int_found
      if ((one_int_found).and.(imin_found)) then
         intersection=.true.
         boundary_index=imin
         d2b=dmin
c     check
         if (d2b.lt.0.0D+0) then
            call error(label)
            write(*,*) 'd2b=',d2b
            write(*,*) '< 0'
            stop
         endif
c     check
         if ((boundary_index.lt.1).or.(boundary_index.gt.1)) then
            call error(label)
            write(*,*) 'imin=',imin
            write(*,*) 'allowed values: 1-1'
            stop
         else
            do j=1,dim
               x(j)=x_list(boundary_index,j)
            enddo               ! j
         endif
c     Debug
         if (debug) then
            write(*,*) 'Intersection found'
            write(*,*) 'boundary_index=',boundary_index
            write(*,*) 'd2b=',d2b
            write(*,*) 'x=',(x(j),j=1,dim)
         endif
c     Debug
      else if ((.not.one_int_found).and.(.not.imin_found)) then
         intersection=.false.
c     Debug
         if (debug) then
            write(*,*) 'NO intersection found'
         endif
c     Debug
      else
         call error(label)
         write(*,*) 'one_int_found=',one_int_found
         write(*,*) 'imin_found=',imin_found
         write(*,*) 'both should agree'
         do i=1,1
            write(*,*) 'int_found(',i,')=',int_found(i)
         enddo                  ! i
         if (one_int_found) then
            write(*,*) 'dmax=',dmax
         endif
         if (imin_found) then
            write(*,*) 'dmin=',dmin
         endif
         stop
      endif

      return
      end
      

      
      subroutine intersection_with_sphere(debug,dim,
     &     x_init,u,delta,x1,radius,
     &     intersection,d2b,x)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to find the intersection between a ray and a radius=R sphere.
c     A solution is returned in the case when the intersection is in
c     the forward direction only.
c     
c     Input:
c       + dim: dimension of space
c       + x_init: initial position
c       + u: propagation direction
c       + delta: displacement
c       + x1: final position
c       + radius: radius of the sphere to intersect
c     
c     Output:
c       + intersection: true if intersection has been found
c       + d2b: distance to boundary
c       + x: final position; is equal to x1 if no intersection has been found
c            is equal to the intersection position otherwise
c     
c     I/O
      logical debug
      integer dim
      double precision x_init(1:Nvec_mx)
      double precision u(1:Nvec_mx)
      double precision delta
      double precision x1(1:Nvec_mx)
      double precision radius
      logical intersection
      double precision d2b
      double precision x(1:Nvec_mx)
c     temp
      logical id
      double precision k0,k1,k2
      double precision v(1:Nvec_mx)
      double precision un(1:Nvec_mx)
      double precision poss(1:Nvec_mx)
      double precision a,b,c
      integer nsol,n_int,j
      double precision P0(1:Nvec_mx)
      double precision P1(1:Nvec_mx)
      double precision P2(1:Nvec_mx)
      double precision pos0(1:Nvec_mx)
      double precision pos1(1:Nvec_mx)
      double precision pos2(1:Nvec_mx)
      double precision flight0(1:Nvec_mx)
      double precision flight1(1:Nvec_mx)
      double precision flight2(1:Nvec_mx)
      double precision rad,theta,phi
      double precision x_spher(1:Nvec_mx)
c     parameters
      double precision epsilonx
      parameter(epsilonx=1.0D-12)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine intersection_with_sphere'

c     Debug
      if (debug) then
         write(*,*) 'This is: ',label(1:strlen(label)),' IN'
      endif
c     Debug
c     Debug
      if (debug) then
         write(*,*) 'radius=',radius
      endif
c     Debug
      
      id=.true.
      do j=1,dim
         if (dabs(x1(j)-x_init(j)).gt.epsilonx) then
            id=.false.
            goto 111
         endif
      enddo                     ! j
 111  continue
c     Debug
      if (debug) then
         write(*,*) 'id=',id
      endif
c     Debug
c
      if (id) then
         intersection=.false.
         d2b=0.0D+0
      else
         call normalize_vector(dim,u,un)
         call spher2cart(x_init(1),x_init(2),x_init(3),
     &        poss(1),poss(2),poss(3))
         a=un(1)**2.0D+0+un(2)**2.0D+0+un(3)**2.0D+0
         b=2.0D+0*(un(1)*poss(1)+un(2)*poss(2)+un(3)*poss(3))
         c=poss(1)**2.0D+0+poss(2)**2.0D+0+poss(3)**2.0D+0
     &        -radius**2.0D+0

         call eq2deg(a,b,c,nsol,k0,k1,k2)

c     Debug
         if (debug) then
            write(*,*) 'nsol=',nsol
         endif
c     Debug
         n_int=nsol
         if (n_int.eq.1) then
            call scalar_vector(dim,k0,un,flight0)
            call add_vectors(dim,poss,flight0,pos0)
            call cart2spher(pos0(1),pos0(2),pos0(3),P0(1),P0(2),P0(3))
            if (dabs(P0(1)-radius).gt.epsilonx) then
               n_int=0
               goto 666
            else
               P0(1)=radius
               if (n_int.eq.1) then
                  call copy_vector(dim,P0,x_spher)
                  intersection=.true.
                  goto 666
               endif
            endif
         else if (n_int.eq.2) then
            call scalar_vector(dim,k1,un,flight1)
            call scalar_vector(dim,k2,un,flight2)
            call add_vectors(dim,poss,flight1,pos1)
            call add_vectors(dim,poss,flight2,pos2)
            call cart2spher(pos1(1),pos1(2),pos1(3),P1(1),P1(2),P1(3))
            call cart2spher(pos2(1),pos2(2),pos2(3),P2(1),P2(2),P2(3))
c     Debug
            if (debug) then
               write(*,*) 'k1=',k1,' k2=',k2
               write(*,*) 'P1=',(P1(j),j=1,dim)
               write(*,*) 'P2=',(P2(j),j=1,dim)
            endif
c     Debug
            if ((k1.ge.0.0D+0).and.(k2.lt.0.0D+0)) then
               P1(1)=radius
               call copy_vector(dim,P1,x_spher)
c     Debug
               if (debug) then
                  write(*,*) 'x_spher=',(x_spher(j),j=1,dim)
               endif
c     Debug
               intersection=.true.
               goto 666
            else if ((k1.lt.0.0D+0).and.(k2.ge.0.0D+0)) then
               P2(1)=radius
               call copy_vector(dim,P2,x_spher)
c     Debug
               if (debug) then
                  write(*,*) 'x_spher=',(x_spher(j),j=1,dim)
               endif
c     Debug
               intersection=.true.
               goto 666
            else
               call error(label)
               write(*,*) 'k1=',k1
               write(*,*) 'k2=',k2
               write(*,*) 'could not decide a solution'
               stop
            endif
         endif                  ! n_int= 1 or 2
      endif                     ! id= T or F

 666  continue

      if (intersection) then
         call cart2spher(x_init(1),x_init(2),x_init(3),
     &        rad,theta,phi)
         d2b=radius-rad
         call spher2cart(x_spher(1),x_spher(2),x_spher(3),
     &        x(1),x(2),x(3))
      endif

c     Debug
      if (debug) then
         write(*,*) 'This is: ',label(1:strlen(label)),' OUT'
      endif
c     Debug
      
      return
      end


      
      subroutine eq2deg(a,b,c,nsol,x0,x1,x2)
      implicit none
      include 'max.inc'
c
c     Purpose: to solve a 2nd degree equation ax�+bx+c=0
c
c     Inputs:
c       + a,b,c: coefficients of the 2nd degree equation ax�+bx+c=0
c
c     Outputs:
c       + nsol: number of solution (0,1 or 2)
c       + x0: solution when nsol=1
c       + x1,x2: solutions when nsol=2 (x1<x2
c
c     I/O
      double precision a,b,c
      integer nsol
      double precision x0,x1,x2
c     temp
      double precision t1,t2
      double precision delta
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine eq2deg'

      delta=b**2.0D+0-4.0D+0*a*c
      if (delta.lt.0.0D+0) then
         nsol=0
      else if (delta.eq.0.0D+0) then
         nsol=1
         x0=-b/(2.0D+0*a)
      else if (delta.gt.0.0D+0) then
         nsol=2
         t1=(-b-dsqrt(delta))/(2.0D+0*a)
         t2=(-b+dsqrt(delta))/(2.0D+0*a)
         if (t1.lt.t2) then
            x1=t1
            x2=t2
         else
            x1=t2
            x2=t1
         endif
      else
         call error(label)
         write(*,*) 'delta=',delta
         stop
      endif

      return
      end
