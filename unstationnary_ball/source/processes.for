      subroutine find_free_process(free,nproc,fpf,proc)
      implicit none
      include 'max.inc'
c
c     Purpose: to find 'proc', the index of a free child process
c
c     Input:
c       + free: T/F value for each child process (0: busy process, 1: free process)
c       + nproc: number of child processes
c
c     Output:
c       + fpf: 0 if no free child process found; 1 otherwise
c       + proc: index of the free child process found, if fpf=1
c
c     I/O
      logical free(1:Nproc_mx)
      integer nproc
      logical fpf
      integer proc
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine find_free_process'

      fpf=.false.
      do proc=1,nproc
         if (free(proc)) then
            fpf=.true.
            goto 123
         endif
      enddo
 123  continue

      return
      end



      subroutine stopped_processes(stopped,nproc,nsp)
      implicit none
      include 'max.inc'
c
c     Purpose: to identify the number of child processes that have been stopped
c
c     Input:
c       + stopped: array of stopped processes (T/F)
c       + nproc: number of child processes
c     
c     Output:
c       + nsp: number of child processes that have been stopped
c     
c     I/O
      logical stopped(1:Nproc_mx)
      integer nproc,nsp
c     temp
      integer proc
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine stopped_processes'
      
      nsp=0
      do proc=1,nproc
         if (stopped(proc)) then
            nsp=nsp+1
         endif
      enddo

      return
      end
