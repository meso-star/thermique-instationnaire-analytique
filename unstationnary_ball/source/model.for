      subroutine Tfluid(debug,dim,time,boundary_index,Tf)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'com_data.inc'
c      
c     Purpose: to compute the temperature of the fluid
c     
c     Input:
c       + dim: dimension of space
c       + time: current time [s]
c       + boundary_index: index of the boundary
c         - boundary_index=1: x=x_min
c         - boundary_index=2: x=x_max
c         - boundary_index=3: y=y_min
c         - boundary_index=4: y=y_max
c         - boundary_index=5: z=z_min
c         - boundary_index=6: z=z_max
c     
c     Output:
c       + time (updated)
c       + Tf: temperature of the fluid @ time
c     
c     I/O
      logical debug
      integer dim
      double precision time
      integer boundary_index
      double precision Tf
c     temp
      double precision S,V
      double precision alpha
      double precision delta_t
      double precision delta_boundary
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine Tfluid'

      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
      endif
c     In this case, the fluid is regulated at a given temperature Text
      if (boundary_index.eq.1) then
         Tf=Text
      else
         call error(label)
         write(*,*) 'boundary_index=',boundary_index
         write(*,*) 'external fluid temperature should never'
         write(*,*) 'be required for this interface'
         stop
      endif
c     This temperature could be a temporal profile so I let "time" as an input
      
      return
      end


      
      subroutine Tradia(debug,dim,time,boundary_index,Tr)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c      
c     Purpose: to compute the radiative temperature
c     
c     Input:
c       + dim: dimension of space
c       + time: current time [s]
c       + boundary_index: index of the boundary
c         - boundary_index=1: x=x_min
c         - boundary_index=2: x=x_max
c         - boundary_index=3: y=y_min
c         - boundary_index=4: y=y_max
c         - boundary_index=5: z=z_min
c         - boundary_index=6: z=z_max
c     
c     Output:
c       + time (updated)
c       + Tr: radiative temperature
c     
c     I/O
      logical debug
      integer dim
      double precision time
      integer boundary_index
      double precision Tr
c     temp
      double precision delta_boundary
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine Tradia'

      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
      endif
c     In this case, the fluid is regulated at a given temperature Text
      if (boundary_index.eq.1) then
         Tr=Trad
      else
         call error(label)
         write(*,*) 'boundary_index=',boundary_index
         write(*,*) 'radiative temperature should never'
         write(*,*) 'be required for this interface'
         stop
      endif
c     This temperature could be a temporal profile so I let "time" as an input

      return
      end

