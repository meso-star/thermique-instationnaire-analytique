      double precision R
      double precision hc
      double precision hr
      double precision epsilon
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision P
      double precision Text
      double precision Trad
      double precision Tinitial
      double precision bfactor
      double precision sfactor
      integer Nevent
      integer seeds(1:Nproc_mx)

      common /com_data1/ R
      common /com_data2/ hc
      common /com_data3/ hr
      common /com_data4/ epsilon
      common /com_data5/ rho_s
      common /com_data6/ Cp_s
      common /com_data7/ lambda_s
      common /com_data8/ P
      common /com_data9/ Text
      common /com_data10/ Trad
      common /com_data11/ Tinitial
      common /com_data12/ bfactor
      common /com_data13/ sfactor
      common /com_data14/ Nevent
      common /com_data15/ seeds
