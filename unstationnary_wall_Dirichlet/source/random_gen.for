      subroutine random_gen(tmp)
      implicit none
      include 'max.inc'
c
c     Main pseudo-random number generator routine
c     (the one that must be called)
c
c     I/O
      double precision tmp
c     temp
      integer n,t
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine random_gen'

      n=1
      t=0
 10   continue
      call zufall(n,tmp)
      if (tmp.eq.0.or.tmp.eq.1) then
         t=t+1
         if (t.gt.10) then
            call error(label)
            write(*,*) 'Too many zeroes'
            stop
         endif
         goto 10
      endif

      return
      end


      subroutine uniform(val1,val2,alpha)
      implicit none
      include 'param.inc'
      include 'max.inc'
c
c     Purpose: to return a value between "val1" and "val2", chosen uniformally
c
c     I/O
      double precision val1,val2,alpha
c     temp
      double precision R
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine uniform'

      if (val2.le.val1) then
         call error(label)
         write(*,*) 'val2=',val2
         write(*,*) '< val1=',val1
         stop
      endif
      call random_gen(R)
      alpha=val1+(val2-val1)*R

      return
      end



      subroutine choose_integer(N1,N2,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to choose an integer between N1 and N2 (included)
c     on a random uniform basis
c
c     Inputs:
c       + N1: minimum value the integer can take
c       + N2: maximum value the integer can take
c
c     Output:
c       + i: integer value between N1 and N2
c
c     I/O
      integer N1,N2,i
c     temp
      double precision R
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine choose_integer'

      call random_gen(R)
      i=N1+int((N2-N1)*R)

      return
      end


      
      subroutine sample_exp(alpha,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to sample a value (double) over the [0,+infty) range
c     according to the following exponential pdf:
c     pdf(x)=alpha*exp(-alpha*x)
c     
c     Input:
c       + alpha: value of the alpha constant
c     
c     Output:
c       + x: chosen value of x
c
c     I/O
      double precision alpha
      double precision x
c     temp
      double precision r
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine sample_exp'

      if (alpha.eq.0.0D+0) then
         call error(label)
         write(*,*) 'alpha=',alpha
         stop
      else
         call random_gen(r)
         x=-dlog(r)/alpha
      endif

      return
      end

      

      subroutine sample_uniform_sphere(u)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to sample a direction uniformly over the unit sphere
c     
c     Input:
c     
c     Output:
c       + u: unit direction sampled uniformly over the unit sphere
c     
c     I/O
      double precision u(1:Nvec_mx)
c     temp
      double precision theta,phi
      double precision r
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine sample_uniform_sphere'
      
      call random_gen(r)
      theta=dasin(2.0D+0*r-1.0D+0)
      call random_gen(r)
      phi=2.0D+0*pi*r
      u(1)=dcos(theta)*dcos(phi)
      u(2)=dcos(theta)*dsin(phi)
      u(3)=dsin(theta)

      return
      end
      
