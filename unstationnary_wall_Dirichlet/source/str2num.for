c     PPart_kdist (http://www.meso-star.com/en_Products.html) - This file is part of PPart_kdist
c     Copyright (C) 2015 - Méso-Star - Vincent Eymet
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine str2num(str,num)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert a character string (that is composed of numeric characters only)
c     into its numeric value
c
c     Input:
c       + str: character string, composed of numeric characters (maximum 10)
c
c     Output:
c       + num: numeric integer value
c

c     input
      character*10 str
c     output
      integer num
c     temp
      integer n,integ,integf
      integer i,j
      character*1 num_ch(0:9),ch
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine str2num'

      num_ch(0)='0'
      num_ch(1)='1'
      num_ch(2)='2'
      num_ch(3)='3'
      num_ch(4)='4'
      num_ch(5)='5'
      num_ch(6)='6'
      num_ch(7)='7'
      num_ch(8)='8'
      num_ch(9)='9'

      n=strlen(str)
      num=0
      do i=1,n
         ch=str(i:i+1)
c     Debug
c         write(*,*) 'i=',i,' ch="',ch(1:strlen(ch)),'"'
c     Debug
         integf=0
         do j=0,9
            if (ch(1:strlen(ch)).eq.num_ch(j)) then
               integf=1
               integ=j
               goto 111
            endif
         enddo ! j
 111     continue
         if (integf.eq.0) then
            call error(label)
            write(*,*) 'character index',i
            write(*,*) 'from string:',str(1:n)
            write(*,*) 'that is:"',ch(1:strlen(ch)),'"'
            write(*,*) 'was not identified as numeric'
            stop
         else
c     Debug
c            write(*,*) 'integ=',integ
c     Debug
            num=num+int(integ*10.0D+0**(n-i))
c     Debug
c            write(*,*) 'num=',num
c     Debug
         endif
      enddo ! i
      
      return
      end
