      subroutine initial_solid_temperature(dim,x,Tinit)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to get the initial temperature at a given position
c     
c     Input:
c       + dim: dimension of space
c       + x: position where the initial temperature must be evaluated
c     
c     Output:
c       + Tinit: initial temperature @ x
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      double precision Tinit
c     temp
      double precision t(1:Nvec_mx)
      double precision d,nf
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine initial_solid_temperature'

      Tinit=Tb10+(Tb20-Tb10)*(x(1)-box(1,1))/(box(1,2)-box(1,1))
c     Debug
c      write(*,*) 'Initial condition was reached'
c     Debug

      return
      end


      
      subroutine normal_function(sigma,d,f)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to evaluate the normal function
c     
c     Input:
c       + sigma: standard deviation
c       + d: distance to center
c     
c     Output:
c       + f: value of the normal function @ d
c     
c     I/O
      double precision sigma
      double precision d
      double precision f
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine normal_function'

      f=dexp(-d**2.0D+0/(2.0D+0*sigma**2.0D+0))
c     &     /dsqrt(2.0D+0*pi*sigma**2.0D+0)

      return
      end
      
