      subroutine intersect(debug,dim,x_init,u,delta,x1,
     &     intersection,boundary_index,d2b,x)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to intersect a ray with a box between x_init and x1
c     
c     Input:
c       + dim: dimension of space
c       + x_init: initial position
c       + u: propagation direction
c       + delta: displacement
c       + x1: final position
c     
c     Output:
c       + intersection: true if intersection has been found
c       + boundary_index: index of the boundary
c         - boundary_index=1: x=x_min
c         - boundary_index=2: x=x_max
c         - boundary_index=3: y=y_min
c         - boundary_index=4: y=y_max
c         - boundary_index=5: z=z_min
c         - boundary_index=6: z=z_max
c       + d2b: distance to boundary
c       + x: final position
c         - is equal to x1 if no intersection has been found
c         - is equal to the intersection position otherwise
c     
c     I/O
      logical debug
      integer dim
      double precision x_init(1:Nvec_mx)
      double precision u(1:Nvec_mx)
      double precision delta
      double precision x1(1:Nvec_mx)
      logical intersection
      integer boundary_index
      double precision d2b
      double precision x(1:Nvec_mx)
c     temp
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine intersect'
      
      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
         write(*,*) 'x_init=',x_init
         write(*,*) 'u=',u
         write(*,*) 'delta=',delta
         write(*,*) 'x1=',x1
      endif

      call forward_intersections(debug,dim,x_init,u,delta,x1,
     &     int_found,d2int,x_list)      
c
c     Check intersections are in the right zones and that the distance to the
c     intersection is smaller than displacement 'delta'
c     
      call filter_intersections_by_zones(debug,dim,
     &     int_found,d2int,x_list)
      call filter_intersections_by_distance(debug,dim,
     &     int_found,d2int,x_list,delta)
c
c     Identify the closest intersection
c
      call find_closest_intersection(dim,debug,
     &     int_found,d2int,x_list,
     &     intersection,boundary_index,d2b,x)
      if (.not.intersection) then
         call copy_vector(dim,x1,x)
      endif

      return
      end



      subroutine forward_intersections(debug,dim,x_init,u,delta,x1,
     &     int_found,d2int,x_list)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c
c     Purpose: to find the intersection between a ray and a (x/y/z)=cst plane.
c     A solution is returned in the case when the intersection is in
c     the forward direction only.
c
c     Input:
c       + dim: dimension of space
c       + x_init: initial position
c       + u: propagation direction
c       + delta: displacement
c       + x1: final position
c     
c     Output:
c       + int_found: true if intersection has been found (for every surface)
c       + d2int: distance to boundary (for every surface)
c       + x_list: intersection position (for every surface)
c     
c     I/O
      logical debug
      integer dim
      double precision x_init(1:Nvec_mx)
      double precision u(1:Nvec_mx)
      double precision delta
      double precision x1(1:Nvec_mx)
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
c     temp
      integer i,j,imin
      double precision x_tmp(1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine forward_intersections'
c
      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
         write(*,*) 'x_init=',x_init,' delta=',delta
      endif

c     Intersection with the box
      do i=1,dim
c     intersection with plane x=box(i,1)
         call intersection_with_plane(debug,dim,
     &        x_init,u,delta,x1,i,box(i,1),
     &        int_found(2*i-1),d2int(2*i-1),x_tmp)
         do j=1,dim
            x_list(2*i-1,j)=x_tmp(j)
         enddo                  ! j
c     intersection with plane x=box(i,2)
         call intersection_with_plane(debug,dim,
     &        x_init,u,delta,x1,i,box(i,2),
     &        int_found(2*i),d2int(2*i),x_tmp)
         do j=1,dim
            x_list(2*i,j)=x_tmp(j)
         enddo                  ! j
      enddo                     ! i
c     
      return
      end



      subroutine filter_intersections_by_zones(debug,dim,
     &     int_found,d2int,x_list)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to filter the intersection list by rectangular zones
c     
c     Intput:
c       + int_found: true if intersection has been found (for every surface)
c       + d2int: distance to boundary (for every surface)
c       + x_list: intersection position (for every surface)
c     
c     Output: same data, updated
c     
c     I/O
      logical debug
      integer dim
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
c     temp
      integer i,j
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine filter_intersections_by_zones'

c     Intersections with the box
      if (int_found(1)) then    ! intersection with plane x=box(1,1)
         if ((x_list(1,2).lt.box(2,1)).or.
     &        (x_list(1,2).gt.box(2,2))) then
            int_found(1)=.false.
         endif
         if ((x_list(1,3).lt.box(3,1)).or.
     &        (x_list(1,3).gt.box(3,2))) then
            int_found(1)=.false.
         endif
      endif
      if (int_found(2)) then    ! intersection with plane x=box(1,2)
         if ((x_list(2,2).lt.box(2,1)).or.
     &        (x_list(2,2).gt.box(2,2))) then
            int_found(2)=.false.
         endif
         if ((x_list(2,3).lt.box(3,1)).or.
     &        (x_list(2,3).gt.box(3,2))) then
            int_found(2)=.false.
         endif
      endif
      if (int_found(3)) then    ! intersection with plane y=box(2,1)
         if ((x_list(3,1).lt.box(1,1)).or.
     &        (x_list(3,1).gt.box(1,2))) then
            int_found(3)=.false.
         endif
         if ((x_list(3,3).lt.box(3,1)).or.
     &        (x_list(3,3).gt.box(3,2))) then
            int_found(3)=.false.
         endif
      endif
      if (int_found(4)) then    ! intersection with plane y=box(2,2)
         if ((x_list(4,1).lt.box(1,1)).or.
     &        (x_list(4,1).gt.box(1,2))) then
            int_found(4)=.false.
         endif
         if ((x_list(4,3).lt.box(3,1)).or.
     &        (x_list(4,3).gt.box(3,2))) then
            int_found(4)=.false.
         endif
      endif
      if (int_found(5)) then    ! intersection with plane z=box(3,1)
         if ((x_list(5,1).lt.box(1,1)).or.
     &        (x_list(5,1).gt.box(1,2))) then
            int_found(5)=.false.
         endif
         if ((x_list(5,2).lt.box(2,1)).or.
     &        (x_list(5,2).gt.box(2,2))) then
            int_found(5)=.false.
         endif
      endif
      if (int_found(6)) then    ! intersection with plane z=box(3,2)
         if ((x_list(6,1).lt.box(1,1)).or.
     &        (x_list(6,1).gt.box(1,2))) then
            int_found(6)=.false.
         endif
         if ((x_list(6,2).lt.box(2,1)).or.
     &        (x_list(6,2).gt.box(2,2))) then
            int_found(6)=.false.
         endif
      endif
c      
      return
      end



      subroutine filter_intersections_by_distance(debug,dim,
     &     int_found,d2int,x_list,delta)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to filter the intersection list with respect to the displacement 'delta'
c     
c     Intput:
c       + int_found: true if intersection has been found (for every surface)
c       + d2int: distance to boundary (for every surface)
c       + x_list: intersection position (for every surface)
c       + delta: displacement
c     
c     Output: (int_found, d2int, x_list) updated
c     
c     I/O
      logical debug
      integer dim
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
      double precision delta
c     temp
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine filter_intersections_by_zones'

      do i=1,2*dim
         if (int_found(i)) then ! intersection with plane x=box(1,1)
            if (d2int(i).gt.delta) then
               int_found(i)=.false.
            endif
         endif
      enddo                     ! i
      
      return
      end      



      subroutine find_closest_intersection(dim,debug,
     &     int_found,d2int,x_list,
     &     intersection,boundary_index,d2b,x)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to identify the closest intersection from the list
c     
c     Intput:
c       + int_found: true if intersection has been found (for every surface)
c       + d2int: distance to boundary (for every surface)
c       + x_list: intersection position (for every surface)
c     
c     Output:
c       + intersection: true if a intersection has been found
c       + boundary_index: index of the boundary where closest intersection is located
c       + d2b: distance to the closest intersection
c       + x: position of the closest intersection
c
c     I/O
      logical debug
      integer dim
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
      logical intersection
      integer boundary_index
      double precision d2b
      double precision x(1:Nvec_mx)
c     temp
      integer i,j,imin
      logical one_int_found,imin_found
      double precision dmin,dmax
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine find_closest_intersection'
      
      dmax=0.0D+0
      one_int_found=.false.
      do i=1,2*dim
         if (int_found(i)) then
            if (d2int(i).ge.dmax) then
               dmax=d2int(i)
               one_int_found=.true.
            endif
         endif
      enddo                     ! i
c     check
      if ((one_int_found).and.(dmax.lt.0.0D+0)) then
         call error(label)
         write(*,*) 'dmax=',dmax
         write(*,*) '< 0'
         stop
      endif
c     check
      imin_found=.false.
      if (one_int_found) then
         dmin=dmax
         do i=1,2*dim
            if (int_found(i)) then
               if ((d2int(i).ge.0.0D+0).and.(d2int(i).le.dmin)) then
                  dmin=d2int(i)
                  imin=i
                  imin_found=.true.
               endif
            endif
         enddo                  ! i
      endif                     ! one_int_found
      if ((one_int_found).and.(imin_found)) then
         intersection=.true.
         boundary_index=imin
         d2b=dmin
c     check
         if (d2b.lt.0.0D+0) then
            call error(label)
            write(*,*) 'd2b=',d2b
            write(*,*) '< 0'
            stop
         endif
c     check
         if ((boundary_index.lt.1).or.(boundary_index.gt.2*dim)) then
            call error(label)
            write(*,*) 'imin=',imin
            write(*,*) 'allowed values: 1-',2*dim
            stop
         else
            do j=1,dim
               x(j)=x_list(boundary_index,j)
            enddo               ! j
         endif
c     Debug
         if (debug) then
            write(*,*) 'Intersection found'
            write(*,*) 'boundary_index=',boundary_index
            write(*,*) 'd2b=',d2b
            write(*,*) 'x=',(x(j),j=1,dim)
         endif
c     Debug
      else if ((.not.one_int_found).and.(.not.imin_found)) then
         intersection=.false.
c     Debug
         if (debug) then
            write(*,*) 'NO intersection found'
         endif
c     Debug
      else
         call error(label)
         write(*,*) 'one_int_found=',one_int_found
         write(*,*) 'imin_found=',imin_found
         write(*,*) 'both should agree'
         do i=1,2*dim
            write(*,*) 'int_found(',i,')=',int_found(i)
         enddo                  ! i
         if (one_int_found) then
            write(*,*) 'dmax=',dmax
         endif
         if (imin_found) then
            write(*,*) 'dmin=',dmin
         endif
         stop
      endif

      return
      end
      


      subroutine intersection_with_plane(debug,dim,
     &     x_init,u,delta,x1,j,c,
     &     intersection,d2b,x)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to find the intersection between a ray and a (x/y/z)=cst plane.
c     A solution is returned in the case when the intersection is in
c     the forward direction only.
c     
c     Input:
c       + dim: dimension of space
c       + x_init: initial position
c       + u: propagation direction
c       + delta: displacement
c       + x1: final position
c       + j: index of the dimension plane to intersect [1-dim]
c       + c: value of (x/y/z) that defines the plane to intersect
c     
c     Output:
c       + intersection: true if intersection has been found
c       + d2b: distance to boundary
c       + x: final position; is equal to x1 if no intersection has been found
c            is equal to the intersection position otherwise
c     
c     I/O
      logical debug
      integer dim
      double precision x_init(1:Nvec_mx)
      double precision u(1:Nvec_mx)
      double precision delta
      double precision x1(1:Nvec_mx)
      integer j
      double precision c
      logical intersection
      double precision d2b
      double precision x(1:Nvec_mx)
c     temp
      double precision k
      double precision v(1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine intersection_with_xplane'

      if ((j.lt.1).or.(j.gt.dim)) then
         call error(label)
         write(*,*) 'Bad input j=',j
         write(*,*) 'should be in the [1-',dim,'] range'
         stop
      endif
      if (x1(j).eq.x_init(j)) then
         intersection=.false.
      else
         k=delta*(c-x_init(j))/(x1(j)-x_init(j))
         if (k.lt.0.0D+0) then
            intersection=.false.
         else
            intersection=.true.
            d2b=k
            call scalar_vector(dim,d2b,u,v)
            call add_vectors(dim,x_init,v,x)
            x(j)=c
         endif
      endif

      return
      end
      

      
      subroutine d2boundary(debug,dim,x_init,u,
     &     intersection,boundary_index,d2b,x)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to get the distance from a position
c     to the boundary in a given direction
c     Only intersections in the forward directions are looked for,
c     with no consideration of distance
c     
c     Input:
c       + dim: dimension of space
c       + x_init: initial position
c       + u: propagation direction
c     
c     Output:
c       + intersection: true if a intersection has been found
c       + boundary_index: index of the boundary where closest intersection is located
c       + d2b: distance to the closest intersection
c       + x: position of the closest intersection
c     
c     I/O
      logical debug
      integer dim
      double precision x_init(1:Nvec_mx)
      double precision u(1:Nvec_mx)
      logical intersection
      integer boundary_index
      double precision d2b
      double precision x(1:Nvec_mx)
c     temp
      integer i
      double precision delta
      double precision t(1:Nvec_mx)
      double precision v(1:Nvec_mx)
      double precision x1(1:Nvec_mx)
      double precision dmax
      logical int_found(1:4*Nvec_mx)
      double precision d2int(1:4*Nvec_mx)
      double precision x_list(1:4*dim,1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine d2boundary'

      do i=1,dim
         t(i)=box(i,2)-box(i,1)
      enddo                     ! i
      call vector_length(dim,t,dmax)
      delta=5.0D+0*dmax
      call scalar_vector(dim,delta,u,v)
      call add_vectors(dim,x_init,v,x1)
c
      call forward_intersections(debug,dim,x_init,u,delta,x1,
     &     int_found,d2int,x_list)
c
c     Check intersections are in the right zones
      call filter_intersections_by_zones(debug,dim,
     &     int_found,d2int,x_list)
c     Identify the closest intersection
      call find_closest_intersection(dim,debug,
     &     int_found,d2int,x_list,
     &     intersection,boundary_index,d2b,x)
c     Debug
      if (.not.intersection) then
         call error(label)
         write(*,*) 'No intersection found'
         write(*,*) 'x_init=',(x_init(i),i=1,dim)
         write(*,*) 'u=',(u(i),i=1,dim)
         write(*,*) '5*dmax=',5*dmax
         stop
      endif
      if (d2b.lt.0.0D+0) then
         call error(label)
         write(*,*) 'd2b=',d2b
         write(*,*) 'boundary_index=',boundary_index
         write(*,*) 'x_init=',(x_init(i),i=1,dim)
         write(*,*) 'u=',(u(i),i=1,dim)
         stop
      endif
c     Debug
         
      return
      end
      
