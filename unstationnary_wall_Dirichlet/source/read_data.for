      subroutine read_data(data_file,dim,
     &     box,rho_s,Cp_s,lambda_s,P,
     &     Tb1,Delta_Tb1,Tb2,
     &     bfactor,sfactor,Nterms,mc,
     &     Nevent,Nprobe,probe_positions,Ntime,time_positions)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c       + dim: dimension of space
c
c     Output:
c       + box: dimensions of the brick
c       + rho_s: density of the solid [kg/m^3]
c       + Cp_s: specific heat of the solid [J/(kg.K)]
c       + lambda_s: thermal conductivity of the solid [W/(m.K)]
c       + P: volumic power density [W/m^3]
c       + Tb1: temperature of left boundary (for t<0) [K]
c       + Delta_Tb1: shift in temperature at the left boundary (from t=0) [K]
c       + Tb2: temperature of right boundary [K]
c       + bfactor: delta_boundary=e/bfactor
c       + sfactor: delta_solid=e/sfactor
c       + Nterms: number of terms in the Fourier series expansion
c       + mc: true if MC computation has to be performed
c       + Nevent: number of statistical realizations
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c     
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      double precision box(1:Nvec_mx,1:2)
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision P
      double precision Tb1
      double precision Delta_Tb1
      double precision Tb2
      double precision bfactor
      double precision sfactor
      integer Nterms
      logical mc
      integer Nevent
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Nt_mx)
      character*(Nchar_mx) infile
c     temp
      integer i,j,ios
      character*(Nchar_mx) nlines_file
      double precision x(1:Nvec_mx)
      logical is_in_box
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=data_file(1:strlen(data_file)),
     &     status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) data_file(1:strlen(data_file))
         stop
      else
         do i=1,6
            read(10,*)
         enddo ! i
         read(10,*) box(1,1)
         read(10,*) box(1,2)
         read(10,*) 
         read(10,*) box(2,1)
         read(10,*) box(2,2)
         read(10,*) 
         read(10,*) box(3,1)
         read(10,*) box(3,2)
         read(10,*)
         read(10,*) rho_s
         read(10,*)
         read(10,*) Cp_s
         read(10,*)
         read(10,*) lambda_s
         read(10,*)
         read(10,*) P
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Tb1
         read(10,*)
         read(10,*) Delta_Tb1
         read(10,*)
         read(10,*) Tb2
         do i=1,5
            read(10,*)
         enddo                  ! i
         read(10,*) bfactor
         read(10,*)
         read(10,*) sfactor
         read(10,*)
         read(10,*) Nterms
         read(10,*)
         read(10,*) mc
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Nevent
      endif
      close(10)
c     Read probe_positions
      infile='./data/probe_positions.in'
c     
      nlines_file='./nlines'
      call get_nlines(infile,nlines_file,Nprobe)
      if (Nprobe.gt.Np_mx) then
         call error(label)
         write(*,*) 'Nprobe=',Nprobe
         write(*,*) 'while Np_mx=',Np_mx
         stop
      endif
c      
      open(11,file=infile(1:strlen(infile)),
     &     status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,Nprobe
            read(11,*) (probe_positions(i,j),j=1,dim)
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) infile(1:strlen(infile))
         stop
      endif
      close(11)
c     
c     Read time_positions
      infile='./data/time_positions.in'
c     
      nlines_file='./nlines'
      call get_nlines(infile,nlines_file,Ntime)
      if (Ntime.gt.Nt_mx) then
         call error(label)
         write(*,*) 'Ntime=',Ntime
         write(*,*) 'while Nt_mx=',Nt_mx
         stop
      endif
c     
      open(11,file=infile(1:strlen(infile)),
     &     status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,Ntime
            read(11,*) time_positions(i)
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) infile(1:strlen(infile))
         stop
      endif
      close(11)
      
c     Inconsistencies
      do i=1,dim
         if (box(i,2).le.box(i,1)) then
            call error(label)
            write(*,*) 'box(',i,',2)=',box(i,2)
            write(*,*) 'should be greater than box(',i,',1)=',box(i,1)
            stop
         endif
      enddo                     ! i
      if (rho_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'rho_s=',rho_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Cp_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Cp_s=',Cp_s
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_s=',lambda_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Tb1.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Tb1=',Tb1
         write(*,*) 'should be positive'
         stop
      endif
      if (Delta_Tb1.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Delta_Tb1=',Delta_Tb1
         write(*,*) 'should be positive'
         stop
      endif
      if (Tb2.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Tb2=',Tb2
         write(*,*) 'should be positive'
         stop
      endif
      if (bfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'bfactor=',bfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (sfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'sfactor=',sfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (Nterms.lt.1) then
         call error(label)
         write(*,*) 'Nterms=',Nterms
         write(*,*) 'should be greater than 0'
         stop
      endif
      if (Nterms.gt.Nterms_mx) then
         call error(label)
         write(*,*) 'Nterms=',Nterms
         write(*,*) 'while Nterms_mx=',Nterms_mx
         stop
      endif
      if (Nevent.le.0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be positive'
         stop
      endif
      do i=1,Nprobe
         do j=1,dim
            x(j)=probe_positions(i,j)
         enddo
         call x_is_in_box(dim,x,is_in_box)
         if (.not.is_in_box) then
            call error(label)
            write(*,*) 'Probe position index:',i
            write(*,*) 'x=',(x(j),j=1,dim)
            write(*,*) 'is not in the box'
            stop
         endif
      enddo                     ! i
      do i=1,Ntime
         if (time_positions(i).lt.0.0D+0) then
            call error(label)
            write(*,*) 'Time index:',i
            write(*,*) 'time_positions(',i,')=',time_positions(i)
            write(*,*) '< 0'
            stop
         endif
      enddo                     ! i
            
      return
      end

