\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}G\IeC {\'e}om\IeC {\'e}trie slab}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Probl\IeC {\`e}me}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Profil de temp\IeC {\'e}rature initial}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Superposition}{4}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Sous-probl\IeC {\`e}me 1: stationnaire}{5}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Sous-probl\IeC {\`e}me 2: instationnaire homog\IeC {\`e}ne}{5}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}R\IeC {\'e}solution du probl\IeC {\`e}me instationnaire homog\IeC {\`e}ne}{6}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}S\IeC {\'e}paration des variables}{6}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Les conditions aux limites}{7}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}Int\IeC {\'e}grale de normalisation}{7}{subsubsection.2.4.3}
\contentsline {subsubsection}{\numberline {2.4.4}Coefficients de la s\IeC {\'e}rie de Fourier}{9}{subsubsection.2.4.4}
\contentsline {subsection}{\numberline {2.5}R\IeC {\'e}sultats}{9}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}D\IeC {\'e}gagement homog\IeC {\`e}ne de puissance volumique}{13}{subsection.2.6}
\contentsline {subsubsection}{\numberline {2.6.1}Modification de la solution stationnaire}{13}{subsubsection.2.6.1}
\contentsline {subsubsection}{\numberline {2.6.2}Modification des coefficients de la s\IeC {\'e}rie de Fourier}{14}{subsubsection.2.6.2}
\contentsline {subsection}{\numberline {2.7}Conditions de type Dirichlet}{16}{subsection.2.7}
\contentsline {section}{\numberline {3}G\IeC {\'e}om\IeC {\'e}trie sph\IeC {\'e}rique}{21}{section.3}
\contentsline {subsection}{\numberline {3.1}Equation de la chaleur en coordonn\IeC {\'e}es sph\IeC {\'e}rique}{21}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Probl\IeC {\`e}me}{22}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}R\IeC {\'e}solution}{22}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Sous-probl\IeC {\`e}me stationnaire}{22}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Sous-probl\IeC {\`e}me instationnaire homog\IeC {\`e}ne}{22}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}R\IeC {\'e}sultats}{24}{subsubsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.4}Prise en compte d'un d\IeC {\'e}gagement de puissance volumique uniforme}{26}{subsubsection.3.3.4}
