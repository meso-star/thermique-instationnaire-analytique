# Thermique instationnaire analytique

Ce projet propose un ensemble de solutions analytiques à des problèmes de thermique instationnaire. Chaque problème fait l'objet d'un code associé qui propose une mise en oeuvre de la solution analytique, et un algorithme statistique, censé retrouver les mêmes solutions.

Les codes proposés dans ce projet sont écrits en (bon vieux) fortran, mais les algorithmes qui sont mis en oeuvre sont ceux de STARDIS.

## Doc

Le répertoire "Doc" contient le pdf où les solutions analytiques sont décrites. Pour l'instant, les solutions sont données pour une géométrie plans-parallèles infinis, et pour une géométrie sphérique.

Dans le cas de la géométrie plans-parallèles infinis (slab), deux sous-problèmes différents sont étudiés:

* un premier problème où l'état initial dans le slab est la solution stationnaire du problème suivant: les parois de gauche et de droite échangent par convection et par rayonnement (les températures de fluide et d'ambiance radiative, les coefficients d'échange convectif et radiatif ayant des valeurs différenciées de chaque côté). A partir de t=0, un flux radiatif connu est imposé sur la paroi de gauche. On désire obtenir le profil de température dans le solide en tout temps T(x,t).

* un second problème où les températures des deux parois sont connues en tout temps: des valeurs sont données pour l'état initial, et un step de température connu est appliqué sur la paroi de gauche à partir de t=0. On désire connaître le profil de température T(x,t).

Dans le cas de la géométrie sphérique, l'état initial est connu (température uniforme) et à partir de t=0, la paroi de la boule échange avec son environnement par convection et rayonnement, sous hypothèse de symétrie sphérique (les conditions aux limites sont uniformes, elles ne dépendent pas de la longitude et de la latitude). On désire connaître le profil de température T(r,t).

Pour chaque problème, la solution est dans un premier temps fournie sur cette base. Dans un second temps, les développements sont repris pour prendre en compte un dégagement de puissance volumique homogène à partir de t=0.

## unstationnary\_wall

Ce répertoire contient le code permettant de calculer la température pour des positions spatiales spécifiées, et des positions temporelles spécifiées, pour le problème numéro 1.1: géométrie slab, profil de température initial résultant de l'état stationnaire d'un problème de conduction / convection / rayonnement, flux radiatif imposé sur la paroi de gauche pour t>0.

## unstationnaly_wall_uniform_Tinit

Ce répertoire contient le code permettant de calculer la température pour des positions spatiales spécifiées, et des positions temporelles spécifiées, pour le problème numéro 1.2: géométrie slab, profil de température initial uniforme, convection et rayonnement imposés pour les deux parois pour t>0 et condition de Neumann (flux net imposé) en plus pour t>0, uniquement d'un côté.

## unstationnary\_wall\_Dirichlet

Ce répertoire contient le code permettant de calculer la température pour des positions spatiales spécifiées, et des positions temporelles spécifiées, pour le problème numéro 2: géométrie slab, profil de température initial linéaire entre deux températures de paroi connues, et application d'un step de température sur la paroi de gauche à t=0.

## unstationnary\_ball

Ce répertoire contient le code permettant de calculer la température pour des positions spatiales spécifiées, et des positions temporelles spécifiées, pour le problème numéro 3: géométrie sphérique, profil de température initial imposé (température uniforme), conditions aux limites de type convection et rayonnement en surface de la boule.

## Utilisation des codes

Les consignes suivantes sont valables pour chaque code:

* compilation (peut nécessiter l'édition du fichier Makefile pour modifier les options de compilation):

> make all

Ces codes sont parallélisés à l'aide d'instructions openmpi: les prérequis sont donc qu'un compilateur fortran soit installé (gfortran) ainsi que les libraires openmpi. Il est également nécessaire de disposer de gnuplot pour la visualisation des résultats.

* Les positions sonde (positions spatiales) où la température doit être évaluée sont fournies au travers du fichier data/probe\_positions.in; les positions temporelles où ces températures sont évaluées sont fournies au travers du fichier data/time\_positions.in
* Les propriétés physiques du problème, ainsi que les paramètres dont la partie Monte-Carlo a besoin, sont fournies au travers du fichier data.in; il est également possible d'activer ou désactiver le calcul MC au travers de ce fichier: dans le cas où le calcul MC est désactivé, seule l'évaluation analytique est effectuée.

* lancement:

> mpirun -np [N] ./probe\_temperature.exe

avec [N] le nombre de threads à utiliser; le schéma de parallélisation est grossier: un processus maître pilote N-1 processus esclaves qui sont chargés du calcul, avant de renvoyer les résultats au processus maître qui se contente finalement de calculer les moyennes et écarts-types; donc en pratique si votre machine peut lancer M threads simultanément, il est pertinent d'utiliser N=M+1 threads pour lancer le code: le processus maître nécessite une puissance de calcul totalement négligeable.

* Les résultats apparaissent dans le répertoire results; un fichier est créé pour chaque position sonde (pour l'ensemble des positions temporelles) et un fichier est créé pour chaque position temporelle (pour l'ensemble des positions sonde). Quelques scripts gnuplot sont fournis pour permettre la visualisation de profils de température temporels. A adapter à vos besoins bien entendu.